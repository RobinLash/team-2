Team Members:

Taylor Sullivan: tsulli16@corning-cc.edu
Robin Lash: rlash@corning-cc.edu
Robert: rclarke2@corning-cc.edu
      : sdavisii@corning-cc.edu
      : mdickin6@corning-cc.edu

Scrum Master: Lead by serving others
- Robert

Product Owner: A point of contact that reviews the product backlog
- Robin
- Product backlog, the to-do list
- A point of contact

Product Backlog:

	Team Intros (x)
	Team Photo  (x)
	Team Name   ()

Brainstorm Ideas:

	Random Name Generator
	+
	Random Number Generator
	- Random Number that pulls a name from a pool
	Hangman Game
	(http://codepen.io/offline_blogger/pen/Kedtr?css-preprocessor=sass)

Assignment: Create Objects Values and Properties for our ideas